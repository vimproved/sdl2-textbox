extern crate sdl2;

use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Point;
use sdl2::rect::Rect;
use sdl2::render::Canvas;
use sdl2::ttf;
use std::time::Duration;

struct Caret {
    head: u32,
    //    tail: u32,
    rect: Rect,
}

impl Caret {
    fn update<T: sdl2::render::RenderTarget>(
        &mut self,
        canvas: &mut Canvas<T>,
        text: &String,
        font: &ttf::Font,
    ) {
        let position = String::from(&text.as_str()[0..self.head.try_into().unwrap()]);
        if position.chars().count() != 0 {
            let surface = font
                .render(&position)
                .blended_wrapped(Color::RGB(255, 255, 255), canvas.output_size().unwrap().0)
                .unwrap();
            let line = position.split("\n").last().unwrap();
            let height = if position.chars().last().unwrap() == '\n' {
                surface.height() as i32
            } else {
                surface.height() as i32 - font.height()
            };
            self.rect.reposition(Point::new(
                font.size_of(&line).unwrap().0 as i32 % canvas.output_size().unwrap().0 as i32,
                height,
            ));
        } else {
            self.rect.reposition(Point::new(0, 0));
        };
        canvas.set_draw_color(Color::RGB(255, 255, 255));
        canvas.fill_rect(self.rect).unwrap();
    }
}

pub fn main() {
    let sdl_context = sdl2::init().unwrap();
    let ttf_context = ttf::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem
        .window("textbox", 800, 600)
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().build().unwrap();

    let font = ttf_context
        .load_font(
            "/usr/share/fonts/nerd-fonts/Hack Regular Nerd Font Complete.ttf",
            24,
        )
        .unwrap();
    let texture_creator = canvas.texture_creator();

    canvas.clear();
    canvas.present();
    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut fulltext = String::new();
    let caret_rect = Rect::new(0, 0, 2, font.height() as u32);
    let mut caret = Caret {
        head: 0,
        //        tail: 0,
        rect: caret_rect,
    };
    'running: loop {
        canvas.set_draw_color(Color::RGB(0, 0, 0));
        canvas.clear();
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'running,
                Event::TextInput { text, .. } => {
                    fulltext.insert_str(caret.head.try_into().unwrap(), &text);
                    println!("{}", &fulltext);
                    caret.head += 1;
                }
                Event::KeyDown {
                    keycode: Some(Keycode::Backspace),
                    ..
                } => {
                    if caret.head > 0 {
                        caret.head -= 1;
                        fulltext.remove(caret.head.try_into().unwrap());
                        println!("{}", &fulltext);
                    }
                }
                Event::KeyDown {
                    keycode: Some(Keycode::Left),
                    ..
                } => {
                    if caret.head > 0 { caret.head -= 1 };
                }
                Event::KeyDown {
                    keycode: Some(Keycode::Right),
                    ..
                } => {
                    if caret.head + 1 > fulltext.chars().count().try_into().unwrap() {
                        break;
                    }
                    caret.head += 1;
                }
                Event::KeyDown {
                    keycode: Some(Keycode::Return),
                    ..
                } => {
                    fulltext.insert_str(caret.head.try_into().unwrap(), "\n");
                    caret.head += 1;
                }
                _ => {}
            }
        }
        if fulltext.chars().count() != 0 {
            let surface = font
                .render(&fulltext)
                .blended_wrapped(Color::RGB(255, 255, 255), canvas.output_size().unwrap().0)
                .unwrap();
            let texture = texture_creator
                .create_texture_from_surface(&surface)
                .unwrap();
            canvas
                .copy(
                    &texture,
                    None,
                    Rect::new(0, 0, surface.width(), surface.height()),
                )
                .unwrap();
            caret.update(&mut canvas, &fulltext, &font);
        }
        canvas.present();
        ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
    }
}
